use nannou::prelude::*;
use rand::Rng;

fn main() {
    nannou::app(model).update(update).simple_window(view).run();
}

// direted lines that have direction they grow in.
struct DirectedLine {
    the_line: Vec<(Point2, Hsl)>,
    direction: Point2,
    color: Hsl, // this the color of the next point in the line.
    color_direction: Hsl,
}

impl DirectedLine {
    fn update_direction(&mut self, gradient: &Box<dyn Fn(Point2) -> Point2>) {
        // get the gradient based on the position of the last point in the_line
        let alteration = gradient(self.the_line.last().unwrap().0);
        // update the direction of the directe line is heading
        self.direction = self.direction + alteration;
    }

    // fn update_color(&mut self) {
    //     let mut rng = rand::thread_rng();
    //     self.color += hsl(
    //         rng.gen_range(-0.1..0.1),
    //         rng.gen_range(-0.1..0.1),
    //         rng.gen_range(-0.1..0.1),
    //     );
    // }

    fn update_color_direction(&mut self, gradient_color: &Box<dyn Fn(Point2, Hsl) -> Hsl>) {
        let alteration = gradient_color(self.the_line.last().unwrap().0, self.color);
        self.color_direction = self.color_direction + alteration;
    }

    fn next_step(&mut self) {
        // increase the_line by another tuple
        self.the_line.push((
            self.the_line.last().unwrap().0 + self.direction,
            self.color + self.color_direction,
        ));
        // remove the first element of the line if the line gets too long
        if self.the_line.len() > 25 {
            self.the_line.remove(0);
        }
    }
}
struct Boundary {
    x_min: f32,
    x_max: f32,
    y_min: f32,
    y_max: f32,
}

struct Model {
    gradient: Box<dyn Fn(Point2) -> Point2>,
    color_gradient: Box<dyn Fn(Point2, Hsl) -> Hsl>,
    vec_lines: Vec<DirectedLine>,
    boundary: Boundary,
}

fn create_gradient(attractors_borrowed: &Vec<(Point2, f32, Hsl)>) -> Box<dyn Fn(Point2) -> Point2> {
    let attractors = attractors_borrowed.to_vec();
    Box::new(move |x: Point2| {
        let mut gradient_direction = pt2(0.0, 0.0);
        for (att_point, att_strength, _color) in attractors.iter() {
            gradient_direction +=
                *att_strength * (*att_point - x) / (pow((*att_point - x).length(), 2));
        }
        return gradient_direction;
    })
}

fn create_color_gradient(
    attractors_borrowed: &Vec<(Point2, f32, Hsl)>,
) -> Box<dyn Fn(Point2, Hsl) -> Hsl> {
    let attractors = attractors_borrowed.to_vec();
    Box::new(move |x: Point2, color: Hsl| {
        let mut change = 0.0;
        let mut my_hue = 0.0;
        let mut my_sat = 0.0;
        let mut my_light = 0.0;
        for (att_point, att_strength, attractor_color) in attractors.iter() {
            // get by how much it should change in that direction
            change = *att_strength; // / (*att_point - x).length();
            change *= 0.2;
            // split the attractor color into components
            let (attract_hue, attract_sat, attract_light) = attractor_color.into_components();
            // add the components to my_*
            let attract_hue_f32 = attract_hue.to_positive_degrees() / 360.0; // neede because hueRGB neq f32
            my_hue += change * attract_hue_f32;
            my_sat += change * attract_sat;
            my_light += change * attract_light;
        }
        let gradient_direction = color - hsl(my_hue, my_sat, my_light);
        return gradient_direction;
    })
}

fn model(app: &App) -> Model {
    // init some kind of random number generator thingy.
    let mut rng = rand::thread_rng();

    // get the dimensions of the box.
    let boundary = Boundary {
        x_min: app.window_rect().x.start,
        x_max: app.window_rect().x.end,
        y_min: app.window_rect().y.start,
        y_max: app.window_rect().y.end,
    };

    let mut my_vec_lines = vec![DirectedLine {
        the_line: vec![
            (
                pt2(
                    rng.gen_range(boundary.x_min..boundary.x_max),
                    rng.gen_range(boundary.y_min..boundary.y_max),
                ),
                hsl(1.0, 0.0, 1.0),
            ),
            // (pt2(-10.3, -40.5), hsl(28.0 / 360.0, 1.0, 0.68)),
        ],
        direction: pt2(rng.gen_range(-1.0..1.0), rng.gen_range(-1.0..1.0)),
        color: hsl(1.0, 0.0, 1.0), // white on a white to black gradient.
        color_direction: hsl(0.0, 0.0, 0.0),
    }];
    // init the lines
    for _num in 1..5000 {
        my_vec_lines.push(DirectedLine {
            the_line: vec![
                (
                    pt2(
                        rng.gen_range(boundary.x_min..boundary.x_max),
                        rng.gen_range(boundary.y_min..boundary.y_max),
                    ),
                    hsl(1.0, 0.0, 1.0), //white
                ),
                // (pt2(-10.3, -40.5), hsl(28.0 / 360.0, 1.0, 0.68)),
            ],
            direction: pt2(rng.gen_range(-1.0..1.0), rng.gen_range(-1.0..1.0)),
            color: hsl(1.0, 0.0, 1.0), //white
            color_direction: hsl(0.0, 0.0, 0.0),
        });
    }
    // init the attractors
    let mut my_attractors = vec![(
        pt2(
            rng.gen_range(boundary.x_min..boundary.x_max),
            rng.gen_range(boundary.y_min..boundary.y_max),
        ),
        rng.gen_range(0.9..1.0),
        hsl(0.3, 0.5, 0.5),
    )];
    // for _num in 1..1 {
    //     my_attractors.push((
    //         pt2(
    //             rng.gen_range(boundary.x_min..boundary.x_max),
    //             rng.gen_range(boundary.y_min..boundary.y_max),
    //         ),
    //         rng.gen_range(0.9..1.0),
    //         hsl(0.3, 0.5, 0.5),
    //     ))
    // }
    Model {
        gradient: create_gradient(&my_attractors),
        color_gradient: create_color_gradient(&my_attractors),
        vec_lines: my_vec_lines,
        boundary: boundary,
    }
}

fn update(_app: &App, model: &mut Model, _update: Update) {
    // model.vec_lines[0].update_direction(&model.gradient);
    // model.vec_lines[0].next_step();
    for line in model.vec_lines.iter_mut() {
        line.update_direction(&model.gradient);
        line.update_color_direction(&model.color_gradient);
        line.next_step();
    }
}

fn view(app: &App, model: &Model, frame: Frame) {
    let draw = app.draw();
    draw.background().color(BLACK);
    for line in model.vec_lines.iter() {
        draw.polyline()
            .weight(1.0)
            .points_colored(line.the_line.iter().copied());
    }
    draw.to_frame(app, &frame).unwrap();
    // to create the png files. takes forever to actually do.
    // let file_path = format!("capture/frame-{:04}.png", frame.nth());
    // app.main_window().capture_frame(file_path);
    //fmpeg command
    // ffmpeg -framerate 60 -i capture/frame-%04d.png -pix_fmt yuv420p -vcodec libx264 -preset veryslow -tune animation -crf 18 -r 60 out.mp4
}
